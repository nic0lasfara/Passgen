package com.nicolas.passgen;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by nicolas on 9/18/17.
 *
 * Controller Implementation
 */

class ControllerImpl implements Controller {

    private Model model;
    private FileOutputStream newSeedFile = null;
    private String patternName = "AbCdEfG1234";

    /**
     * Constructor.
     *
     */
    ControllerImpl() {
        this.model = new ModelImpl();
    }

    /**
     * @throws NullPointerException .
     */
    @Override
    public void checkFile(final Context context) throws NullPointerException {
        FileInputStream seedFile;

        try {
            seedFile = context.openFileInput("seed2");
            InputStreamReader isr = new InputStreamReader(seedFile, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            Log.i("ReadFile", sb.toString());
            if (sb.toString().compareTo("") == 0)
                throw new NullPointerException();

        } catch (FileNotFoundException e) {
            Log.i("FileNotFound", e.toString());
            try {
                this.newSeedFile = context.openFileOutput("seed2", Context.MODE_PRIVATE);
                this.newSeedFile.write(patternName.getBytes());
                this.newSeedFile.close();
            } catch (IOException g) {
                Log.i("IoNewFile", g.toString());
            }

        } catch (UnsupportedEncodingException e) {
            Log.i("Encoding", e.toString());

        } catch (IOException e) {
            Log.i("IOException", e.toString());
        } catch (NullPointerException e) {
            throw new NullPointerException();
        } catch (Exception e) {
            Log.i("Exception", e.toString());
        }
    }

    /**
     * .
     * @param context c.
     * @throws FileNotFoundException e.
     */
    public void insertSeed(final Context context) throws FileNotFoundException {
        try {
            this.newSeedFile = context.openFileOutput("seed2", Context.MODE_PRIVATE);
            this.newSeedFile.write(patternName.getBytes());
            this.newSeedFile.close();

        } catch (FileNotFoundException e) {
            Log.i("insertSeedFile", e.toString());

        } catch (IOException e) {
            Log.i("insertSeedIO", e.toString());
        }
    }


    /**
     * @param name   Name of service.
     * @param length Length of the password.
     * @return The password.
     */
    @Override
    public String callGeneratedPassword(final String name,
                                        final short length) {

        return model.generatePassword(name, this.patternName, length);
    }
}
