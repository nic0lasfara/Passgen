package com.nicolas.passgen;

/**
 * Created by nicolas on 9/18/17.
 *
 */

interface Model {
    /**
     * This method generate the password.
     *
     * @param name   Name of the service.
     * @param seed   Seed to cipher the password.
     * @param length length of the password.
     * @return The password.
     */
    String generatePassword(String name, String seed, short length);
}
