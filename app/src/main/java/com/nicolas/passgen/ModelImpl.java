package com.nicolas.passgen;

import java.security.MessageDigest;

/**
 * Created by nicolas on 9/18/17.
 *
 */

class ModelImpl implements Model {
    /**
     * This method generate the password.
     *
     * @param name   Name of the service.
     * @param seed   Seed to cipher the password.
     * @param length length of the password.
     * @return The password.
     */
    @Override
    public String generatePassword(final String name, final String seed,
                                   final short length) {
        return sha256(name + seed + Short.toString(length));
    }

    /**
     * .
     * @param base .
     * @return .
     */
    private static String sha256(final String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();
            String hex;

            for (byte i : hash) {
                hex = Integer.toHexString(0xFF & i);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();

        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
