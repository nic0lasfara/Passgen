package com.nicolas.passgen;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.content.DialogInterface;

import java.io.FileNotFoundException;


/**
 * This class create and set the view.
 */
public class MainActivityView extends AppCompatActivity {

    private Controller controller;
    private TextView generatedPassword;
    private EditText inputService;
    private final String ERROR = "Error";



    public MainActivityView() {
        this.controller = new ControllerImpl();
    }

    /**
     * This method create the view, attach the components and call
     * method for refreshing them.
     *
     * @param savedInstanceState Instance.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view);
        SeekBar lengthPassword;

        this.generatedPassword = (TextView) findViewById(R.id.pswbox);
        this.inputService = (EditText) findViewById(R.id.servicename);
        lengthPassword = (SeekBar) findViewById(R.id.lengthpswd);

        try {
            this.controller.checkFile(getApplicationContext());
        } catch (NullPointerException e) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivityView.this);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setTitle("Warning");
            dialogBuilder.setMessage("Attention, seed not found on the file.\nNew seed has been generated " +
                    "All password generated before are not valid now");
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    Log.i("PopUP", "ok");
                }
            });

            AlertDialog dialog = dialogBuilder.create();
            dialog.show();
            try {
                this.controller.insertSeed(getApplicationContext());
            } catch (FileNotFoundException g) {
                Log.i("FileNotFound", g.toString());
            }
        }

        lengthPassword.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private int currentLength = 0;

            @Override
            public void onProgressChanged(final SeekBar seekBar, final int i, final boolean b) {
                this.currentLength = i;

                if (getInputService().getText().toString().length() == 0) {
                    getGeneratedPassword().setText(ERROR);
                } else {
                    getGeneratedPassword().setText(controller.callGeneratedPassword(
                            getInputService().getText().toString(), (short) this.currentLength));
                }

                Log.i("Length", getInputService().getText().toString());

            }

            @Override
            public void onStartTrackingTouch(final SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(final SeekBar seekBar) {

            }
        });

    }

    /**
     * @return .
     */
    public TextView getGeneratedPassword() {
        return generatedPassword;
    }

    public EditText getInputService() {
        return inputService;
    }

}


