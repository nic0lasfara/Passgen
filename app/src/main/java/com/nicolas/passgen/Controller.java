package com.nicolas.passgen;

import android.content.Context;

import java.io.FileNotFoundException;

/**
 * Created by nicolas on 9/18/17.
 * This interface define the public method for the controller.
 */
interface Controller {
    /**
     * This method check if is present in the system the file.
     * @param context Context.
     * @throws NullPointerException on read file.
     */
    void checkFile(Context context) throws NullPointerException;

    /**
     * Call the main method for this application.
     *
     * @param name   Name of service.
     * @param length Length of the password.
     * @return The password.
     */
    String callGeneratedPassword(String name, short length);

    /**
     * .
     * @param context .
     * @throws FileNotFoundException .
     */
    void insertSeed(Context context) throws FileNotFoundException;
}
